@include('includes.head')
<body>
<!-- Header start -->
@include('includes.header')

<!-- Page Title start -->
<div class="pageTitle">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6">
        <h1 class="page-heading">Job Listing</h1>
      </div>
      <div class="col-md-6 col-sm-6">
        <div class="breadCrumb"><a href="#.">Home</a> / <a href="#.">Job Search</a> / <span>Job Name</span></div>
      </div>
    </div>
  </div>
</div>
<!-- Page Title End -->

<div class="listpgWraper">
  <div class="container"> 
    
    
    <!-- Search Result and sidebar start -->
    <div class="row">
      <div class="col-md-3 col-sm-6"> 
        <!-- Side Bar start -->
        <div class="sidebar"> 
          <!-- Jobs By Title -->
          <div class="widget">
            <h4 class="widget-title">Jobs By Title</h4>
            <ul class="optionlist">
              <li>
                <input type="checkbox" name="checkname" id="webdesigner" />
                <label for="webdesigner"></label>
                Web Designer <span>12</span> </li>
              <li>
                <input type="checkbox" name="checkname" id="3dgraphic" />
                <label for="3dgraphic"></label>
                3D Graphic Designer <span>33</span> </li>
              <li>
                <input type="checkbox" name="checkname" id="graphic" />
                <label for="graphic"></label>
                Graphic Designer <span>33</span> </li>
              <li>
                <input type="checkbox" name="checkname" id="electronicTech" />
                <label for="electronicTech"></label>
                Electronics Technician <span>33</span> </li>
              <li>
                <input type="checkbox" name="checkname" id="webgraphic" />
                <label for="webgraphic"></label>
                Web / Graphic Designer <span>33</span> </li>
              <li>
                <input type="checkbox" name="checkname" id="brandmanager" />
                <label for="brandmanager"></label>
                Brand Manager <span>33</span> </li>
            </ul>
         </div>
          

        </div>
        <!-- Side Bar end --> 
      </div>
      <div class="col-md-3 col-sm-6 pull-right"> 
        <!-- Social Icons -->
        <div class="sidebar">
          <h4 class="widget-title">Follow Us</h4>
          <div class="social"> <a href="http://www.twitter.com" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a> <a href="http://www.plus.google.com" target="_blank"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a> <a href="http://www.facebook.com" target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a> <a href="https://www.pinterest.com" target="_blank"><i class="fa fa-pinterest-square" aria-hidden="true"></i></a> <a href="https://www.youtube.com" target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a> <a href="https://www.linkedin.com" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a> </div>
          <!-- Social Icons end --> 
        </div>
        
        <!-- Sponsord By -->
        <div class="sidebar">
          <h4 class="widget-title">Sponsord By</h4>
          <div class="gad"><img src="images/googlead.jpg" alt="your alt text" /></div>
          <div class="gad"><img src="images/googlead.jpg" alt="your alt text" /></div>
          <div class="gad"><img src="images/googlead2.jpg" alt="your alt text" /></div>
          <div class="gad"><img src="images/googlead2.jpg" alt="your alt text" /></div>
        </div>
      </div>
      <div class="col-md-6 col-sm-12"> 
        <!-- Search List -->
        <ul class="searchList">
          @if(count($events) >0 )
              @foreach($events as $e)
              <!-- job start -->
                <li>
                  <div class="row">
                    <div class="col-md-8 col-sm-8">
                      <div class="jobimg"><img src="images/jobs/jobimg.jpg" alt="Job Name" /></div>
                      <div class="jobinfo">
                      <h3><a href="#.">{{$e->title}}</a></h3>
                      
                      <div class="location">  - <span>{{$e->location}}</span></div>
                      </div>
                      @if(Auth::user())
                    <input type="hidden" name="userLog" id="userLog" value="1">
                    @else
                    <input type="hidden" name="userLog" id="userLog" value="0">
                    @endif
                      <div class="clearfix"></div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                      <div class="listbtn"><a href="#.">Apply Now</a></div>
                    </div>
                  </div>
                <p>{{ substr(strip_tags($e->description),0,150) }}</p>
                </li>
              <!-- job end --> 
              @endforeach
            @endif
        </ul>
       
      </div>
    </div>
  </div>
</div>

@include('includes.footer')
<script>
  var base_url = '{!! url('/login') !!}';
    $(document).ready(function(){
        $('.listbtn').on('click',function(){
            if($('#userLog').val()== 0){  
              window.location.href = base_url;
            }
           

       });
        
       
    });
</script>
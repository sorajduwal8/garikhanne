@component('mail::message')
# Introduction

The body of your message.
Password : {{$password}}
 User: {{$user['email']}}

@component('mail::button', ['url' => 'http://localhost/naradmuni/public/login'])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

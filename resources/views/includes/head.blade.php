
  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Online Job Portal HTML</title>
<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('asset/css/magnific-popup.css')}}">
<link rel="stylesheet" href="{{asset('asset/css/jquery-ui.css')}}">
<link rel="stylesheet" href="{{asset('asset/css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('asset/css/owl.theme.default.min.css')}}">
<link rel="stylesheet" href="{{asset('asset/css/bootstrap-datepicker.css')}}">
<link rel="stylesheet" href="{{asset('asset/css/mediaelementplayer.css')}}">
<link rel="stylesheet" href="{{asset('asset/css/animate.css')}}">
<link rel="stylesheet" href="{{asset('asset/fonts/flaticon/font/flaticon.css')}}">
<link rel="stylesheet" href="{{asset('asset/css/fl-bigmug-line.css')}}">
<link rel="stylesheet" href="{{asset('asset/css/materrials.scss')}}">
<!-- Custom Style -->
<link href="{{asset('css/main.css')}}" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="js/html5shiv.min.js"></script>
  <script src="js/respond.min.js"></script>
<![endif]-->

</head>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>Narad Muni</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description"/>
    <meta content="Coderthemes" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    @include('admin.include.styles')

</head>
<body>
<div id="wrapper">
    @include('admin.include.header')
    @include('admin.include.sidebar')
    <div class="content-page">
        <div class="content">
            <div class="container-fluid">

                @yield('content')
            </div>
        </div>
    </div>
    @include('admin.include.footer')
</div>
@include('admin.include.scripts')

<script>
    var baseUrl = "{{url('/')}}";
</script>

@yield('scripts')

</body>

</html>

<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">

    <div class="slimscroll-menu">

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">

                <li class="menu-title">Navigation</li>
                <li><a href="{{url('/')}}" target="_blank" >Website</a></li>

                <li>
                    <a href="javascript: void(0);">
                        <i class="fe-airplay"></i>
                        <span class="badge badge-success badge-pill float-right"></span>
                        <span> Users </span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="{{url('/admin/users')}}">User list</a>
                        </li>
                        <li>
                            <a href="{{url('/admin/invited-user')}}"> Invited User list</a>
                        </li>

                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);">
                        <i class="fe-flag"></i>
                        <span class="badge badge-success badge-pill float-right"></span>
                        <span> Banner </span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="{{url('/admin/banner')}}">Banner list</a>
                        </li>
            
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);">
                        <i class="fe-shopping-cart"></i>
                        <span class="badge badge-success badge-pill float-right"></span>
                        <span> Events </span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="{{url('/admin/event')}}">Events list</a>
                        </li>
            
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);">
                        <i class="fe-flag"></i>
                        <span class="badge badge-success badge-pill float-right"></span>
                        <span> Blogs </span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="{{url('/admin/blog')}}">Blog list</a>
                        </li>
            
                    </ul>
                </li>
            </ul>

        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->

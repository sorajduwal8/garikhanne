<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Event;
class EventController extends Controller
{
    public function index()
    {
        $events = Event::all();
        return view('admin.event.index', compact('events'));
        
    }
    public function create()
    {
        
        return view('admin.event.create');
    }

    public function store(Request $request)
    {

        $request->validate([
           
            'image' => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title' => 'required'
        ]);
         
    
        $data['title'] = $request->get('title');
        $data['description'] = $request->get('description');
        $data['date'] = $request->get('date');
        $data['meta_title'] = $request->get('meta_title');
        $data['meta_description'] = $request->get('meta_desc');
      
        $data['location'] = $request->get('location');
        if($file = $request->file('image')) {
            $name = time().time().'.'.$file->getClientOriginalExtension();
            $target_path = public_path('/images/event/');
           
                if($file->move($target_path, $name)) {
                   
                    $data['image']  = $name;
                }
            }
       
         Event::create($data);
    
        
         $request->session()->flash('success','Event created');
         return redirect('admin/event');
    }

    public function edit($id)
    {
        $purpose = '';
        $event =  Event::findOrfail($id);
            
     
        return view('admin.event.create', compact('event','purpose'));

    }
    public function update(Request $request, $id)
    {
      
        
        $request->validate([
           
            'image' => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            
        ]);


        $event = Event::findOrFail($id);
       
        $event->title = $request->get('title');
        $event->description = $request->get('description');
        $event->date = $request->get('date');
        $event->location = $request->get('location');
        $event->meta_title = $request->get('meta_title');
        $event->meta_description = $request->get('meta_description');
        if($file = $request->file('image')) {
            $name = time().time().'.'.$file->getClientOriginalExtension();
            $target_path = public_path('/images/event/');
            
                if($file->move($target_path, $name)) {
                    $event->image  = $name;
                }
            }
            $event->update();
            $request->session()->flash('success','Event Updated');

            return redirect('admin/event');
           


    }

    public function delete($id, Request $request)
    {
        $event = Event::findOrFail($id);
        $event->delete();
        $request->session()->flash('success','Event deleted');
        return redirect('admin/event');
    }
}

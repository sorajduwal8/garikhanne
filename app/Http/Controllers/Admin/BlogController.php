<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Blog;

class BlogController extends Controller
{
    public function index()
    {
       $blogs = Blog::all();
       return view('admin.blog.index', compact('blogs'));

    }

    public function create()
    {
        return view('admin.blog.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description' =>'required'
        ]);
       
        $blog = new Blog();
        $blog->title = $request->get('title');
        $blog->description = $request->get('description');
        if($file = $request->file('image')) {
            $name = time().time().'.'.$file->getClientOriginalExtension();
            $target_path = public_path('/images/blog/');
           
                if($file->move($target_path, $name)) {
                    $blog->image  = $name;
                }
            }
           
         $blog->save();
         $request->session()->flash('success','blog created');

         return redirect('admin/blog');

        
    }

    public function edit($id)
    {
        $purpose = '';
        $blog = Blog::findOrFail($id);
        return view('admin.blog.create', compact('blog','purpose'));
        
    }
    public function update(Request $request, $id)
    {
       
        $request->validate([
           
            'image' => 'sometimes|required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title' => 'required'
        ]);
        $blog = Blog::findOrfail($id);
        $blog->title = $request->title;
        $blog->description = $request->get('description');
        if($file = $request->file('image')) {
            $name = time().time().'.'.$file->getClientOriginalExtension();
            $target_path = public_path('/images/blog/');
            
                if($file->move($target_path, $name)) {
                    $blog->image   = $name;
                }
            }
            else
            {
                $blog->image = $blog->image;

            }
            $blog->update();
            $request->session()->flash('success','Blog Updated');

            return redirect('admin/blog');

    }

    public function delete($id, Request $request)
    {
        $blog = Blog::findOrFail($id);
        $blog->delete();
        $request->session()->flash('success','Blog deleted');
        return redirect('admin/blog');
    }
}

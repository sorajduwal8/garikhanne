<?php

namespace App\Models;

use App\Events\NewUserRegisterEvent;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table='users';
    protected $fillable =['id','name','email','password','role'];

    protected $dispatchesEvents = [
        'created' => NewUserRegisterEvent::class,
    ];
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/','HomeController@index');
//Route::get('/', function () {
//    return redirect('/admin/dashboard');
//});

Auth::routes();
//
//Route::get('/home', 'dashboardController@index')->name('home');
Route::get('/admin', function () {
    return redirect('/admin/dashboard');
});
Route::get('/home', function () {
    return redirect('/admin/dashboard');
});

Route::get('/admin/home', function () {
    return redirect('/admin/dashboard');
});
Route::get('redirect/{provider}','Frontend\SocialLoginController@redirect');
    Route::get('callback/{provider}','Frontend\SocialLoginController@callback');

Route::group(['middleware' => ['auth'], 'namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::get('dashboard', 'DashboardController@index');
    Route::get('users', 'UserController@index');
    Route::get('users/create', 'UserController@create');
    Route::post('users/store' , 'UserController@store');
    Route::get('users/{id}/edit', 'UserController@edit');
    Route::post('users/store/{id}', 'UserController@update');
    Route::get('users/{id}/delete', 'UserController@delete');
    Route::delete('users/{id}/delete', 'UserController@delete');
    Route::get('changeStatus','UserController@changestatus');

    Route::get('menu','MenuController@menu')->name('menu');
    Route::get('/wmenuindex', array('as' => 'wmenuindex','uses'=>'MenuController@wmenuindex'));

Route::post('/addcustommenur', array('as' => 'addcustommenur','uses'=>'MenuController@addcustommenu'));
Route::post('/deleteitemmenur', array('as' => 'deleteitemmenur','uses'=>'MenuController@deleteitemmenu'));
Route::post('/deletemenugr', array('as' => 'deletemenugr','uses'=>'MenuController@deletemenug'));
Route::post('/createnewmenur ', array('as' => 'createnewmenur ','uses'=>'MenuController@createnewmenu'));
Route::post('/generatemenucontrolr', array('as' => 'generatemenucontrolr','uses'=>'MenuController@generatemenucontrol'));
Route::post('/updateitemr', array('as' => 'updateitemr','uses'=>'MenuController@updateitem'));
Route::get('/addcustommenur', array('as' => 'addcustommenur','uses'=>'MenuController@addcustommenu'));
Route::get('/deleteitemmenur', array('as' => 'deleteitemmenur','uses'=>'MenuController@deleteitemmenu'));
Route::get('/deletemenugr', array('as' => 'deletemenugr','uses'=>'MenuController@deletemenug'));
Route::get('/createnewmenur ', array('as' => 'createnewmenur ','uses'=>'MenuController@createnewmenu'));
Route::get('/generatemenucontrolr', array('as' => 'generatemenucontrolr','uses'=>'MenuController@generatemenucontrol'));
Route::post('/updateitemr', array('as' => 'updateitemr','uses'=>'MenuController@updateitem'));


    Route::get('invite', 'InviteController@create');
    Route::get('invited-user', 'InviteController@index');
    Route::post('invite/store' , 'InviteController@store');
    Route::delete('invite/{id}/delete', 'InviteController@delete');

    Route::get('banner', 'BannerController@index');
    Route::get('banner/create','BannerController@create');
    Route::get('banner/{id}/edit','BannerController@edit');
    Route::post('banner/store' ,'BannerController@store');
    Route::post('banner/store/{id}','BannerController@update');
    Route::get('banner/{id}/delete', 'BannerController@delete');

    Route::get('blog', 'BlogController@index');
    Route::get('blog/create','BlogController@create');
    Route::get('blog/{id}/edit','BlogController@edit');
    Route::post('blog/store' ,'BlogController@store');
    Route::post('blog/store/{id}','BlogController@update');
    Route::get('blog/{id}/delete', 'BlogController@delete');

    Route::get('event', 'EventController@index');
    Route::get('event/create','EventController@create');
    Route::get('event/{id}/edit','EventController@edit');
    Route::post('event/store' ,'EventController@store');
    Route::post('event/store/{id}','EventController@update');
    Route::get('event/{id}/delete', 'EventController@delete');



    



});
